﻿<html>
 <head>
 <meta charset="utf-8">
  <body>
  <style>
		td {
   border: 1px solid grey;
   text-align: center;
   min-width: 100px;
}

		</style>

<?php
	include_once("data.php");
	require('db.php');
	mysqli_query($mysql,"SET NAMES 'utf8'");
	$data = loaddata();
	$i = 1;
	$table = '<table>';
	foreach ($data as $line)
	{
		if(strlen($line['name']) == 0)
		{
			echo $line['name'].' '. $line['mark'].' '. $line['model'].' '. $line['kuzov'].' '. $line['fr'].' '. $line['lr'].' '. $line['ud'].' -  <span style = "color:red"> Нет названия</span><br>';
			continue;
		}
		$res = mysqli_query( $mysql, "SELECT `CATEGORY` as `cat`, `NAME` as `name` FROM `category` WHERE `NAME` = '".$line['name']."'");
		if($res)
		{
			$rows = mysqli_fetch_assoc($res);
			if($rows['name'] == $line['name'])
			{
				$res = mysqli_query( $mysql, "SELECT MAX(`BARCODE`)+1 as `handle` from `barcode`");
				if($res)
				{
					$row = mysqli_fetch_assoc($res);
					$row['handle'] = str_pad($row['handle'], 9, "0", STR_PAD_LEFT);
					$sqlline = "
					INSERT INTO `barcode` 
					(
						`BARCODE`, 
						`IS_ENABLE`, 
						`O_N`, 
						`CATEGORY`, 
						`DEFECT`, 
						`PRICE_REC_USD`, 
						`PRICE_REC_RUB`, 
						`FIELD1`, 
						`FIELD2`, 
						`FIELD3`, 
						`FIELD4`, 
						`FIELD6`, 
						`FIELD7`, 
						`FIELD8`, 
						`FIELD9`, 
						`FIELD10`, 
						`FIELD11`, 
						`FIELD12`, 
						`FIELD13`, 
						`REMARK`, 
						`SUPPLY`, 
						`COST_USD`, 
						`COST_RUB`, 
						`MAN_INS`, 
						`MAN_UPD`, 
						`DATE_INS`, 
						`DATE_UPD`, 
						`HAVE_PHOTO`, 
						`LAST_OPERATION`, 
						`send_act`, 
						`send_flag`, 
						`send_skip`, 
						`send_arc`, 
						`send_last`, 
						`send_edited`, 
						`send_photo`, 
						`video_url`, 
						`warning`
					) 
					VALUES 
					(
						'".$row['handle']."', 
						'1', 
						'O', 
						'".$rows['cat']."', 
						'0', 
						NULL, 
						NULL, 
						'".str_replace("_", " ", $line['mark'])."', 
						'".str_replace("_", " ", $line['model'])."',
						'".str_replace("_", " ", $line['kuzov'])."', 
						'".$line['num']."', 
						'".$line['fr']."',  
						'".$line['lr']."',  
						'".$line['ud']."',
						NULL, 
						NULL, 
						'".mb_strtolower($line['oem'])."', 
						'".$line['color']."', 
						NULL, 
						'".$line['comm']."', 
						'001', 
						NULL, 
						NULL, 
						'', NULL, 
						current_timestamp(), 
						current_timestamp(), 
						'0', 
						'INS', 
						'add', 
						'0', 
						'0', 
						'0', 
						'', 
						'', 
						'1', 
						NULL, 
						''
					);";
					$res = mysqli_query( $mysql, $sqlline);
					if($res)
					{
						$query = "INSERT INTO `operation`(`OPERATION`,`BARCODE`,`OPERATION_TYPE`,`MAN_GRANT`,`IS_LAST`,`ACCOUNT`,`PRICE_USD`,`PRICE_RUB`,`REMARK`,`DATE_INS`)VALUES(NULL,'".$row['handle']."','INS','adm','0',NULL,	NULL,NULL,NULL,current_timestamp());";
						$res = mysqli_query( $mysql, $query);
						if($res)
						{	 
							$table = $table."<tr><td>".$row['handle']."</td><td>". $line['name'].'</td><td>'. $line['mark'].'</td><td>'. $line['model'].'</td><td>'. $line['kuzov'].'</td><td>'. $line['fr'].'</td><td>'. $line['lr'].'</td><td>'. $line['ud'].'</td><td>'.$line['num']. '</td><td>'.$line['comm'].'</td></tr>';
							addphoto($row['handle'], $line['image1'], 1);
							addphoto($row['handle'], $line['image2'], 2);
							addphoto($row['handle'], $line['image3'], 3);
							addphoto($row['handle'], $line['image4'], 4);
							deleteline($line['handle']);
							$i ++;
						}
						else
						{
							echo "[".$row['handle']."] ". $line['name'].' '. $line['mark'].' '. $line['model'].' '. $line['kuzov'].' '. $line['fr'].' '. $line['lr'].' '. $line['ud'].' -  <span style = "color:red"> ошибка добавления опирации</span><br>';
						}
					}
					else
					{
						echo
						 	"[".
								$row['handle'].
							"] ". 
								$line['name'].' '.
								$line['mark'].' '.
								$line['model'].' '.
								$line['kuzov'].' '.
								$line['fr'].' '.
								$line['lr'].' '.
								$line['ud'].' - <span style = "color:red"> ошибка добавления</span> <br> Ошибка: '. mysqli_error($mysql);
					}
				}
			}
			else
			{
				echo $line['name'].' '. $line['mark'].' '. $line['model'].' '. $line['kuzov'].' '. $line['fr'].' '. $line['lr'].' '. $line['ud'].' -  <span style = "color:red"> название "'.$line['name'].'" не найдено</span><br>';
			}
		}
		else
		{
			echo $line['name'].' '. $line['mark'].' '. $line['model'].' '. $line['kuzov'].' '. $line['fr'].' '. $line['lr'].' '. $line['ud'].' -  <span style = "color:red">запчасть не добавлена, название не найдено</span><br>';
		}
	}
	$table = $table.'</table>';
	echo $table;
	logadden(date("Y-m-d H:i:s")."+7 часов");
	logadden($table);
	function addphoto($barcode, $name, $num)
	{
		global $mysql;
		if(strlen($name) <= 2) return 1;
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.$name))
		{
			$f = fopen($_SERVER['DOCUMENT_ROOT'].'/images/'.$name, 'rb');
			if(!$f) return 1;
			$img = fread($f, 1048576);
			fclose($f);
			$query = "INSERT INTO `photo` 
			(
				`BARCODE`, 
				`PHOTO_NUM`, 
				`DATE_INS`, 
				`IS_PREVIEW`, 
				`MAN_INS`, 
				`IMAGE`,
				`DATE_UPD`
			) 
			VALUES 
			(
				'".$barcode."', 
				'".$num."', 
				current_timestamp(), 
				'".($num==1?1:0)."', 
				'adm',
				'".addslashes($img)."', 
				current_timestamp()
			)";
			//echo $query.'<br>';
			//".addslashes($img)."
			$res = mysqli_query( $mysql, $query);
			//$resa = mysqli_query($mysql, "SELECT MAX(`BARCODE`) FROM `photo`");
			//echo "!!!!".var_dump(mysqli_insert_id($mysql))."<br>";
			if($res) 
			{
				$opq = "
					INSERT INTO `operation` 
					(
						`BARCODE`, 
						`OPERATION_TYPE`, 
						`MAN_GRANT`, 
						`IS_LAST`, 
						`ACCOUNT`, 
						`PRICE_USD`, 
						`PRICE_RUB`, 
						`REMARK`, 
						`DATE_INS`
					) 
					VALUES 
					(
						'".$barcode."', 
						'PHOTOINS', 
						'adm', 
						'0', 
						NULL, 
						NULL, 
						NULL, 
						'Вставка фото №".$num."', 
						current_timestamp()
					)";
				$res = mysqli_query( $mysql, $opq);
				if(mysqli_insert_id($mysql) != 0)
				{
					echo "<span style = 'color: green'>[".$barcode."] Фото добавлено!</span><br>";
				}
				else 
				{
					echo "<span style = 'color: red'>Ошибка вставки операций в [".$barcode."] #Фото: ".$num."</span><br>";
					echo "<span style = 'color: yellow'>[MySQL] - ".mysqli_error($mysql)."</span><br>";
				}
				return 1;
			}
			else 
			{
				echo "<span style = 'color: red'>Ошибка вставки фото в [".$barcode."] #Фото: ".$num." название файла \"".$name."\"</span><br>";
			}
			echo mysqli_error($mysql);
		}
		else
		{
			echo '<span style = "color: red">Фотография "images/'.$name.'" не найдена</span>';
		}
		return 0;
	}
	function getcountphoto($imgname1, $imgname2, $imgname3, $imgname4)
	{
		$count = 0;
		if(strlen($imgname1) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.$imgname1)) $count ++;
		if(strlen($imgname2) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.$imgname2)) $count ++;
		if(strlen($imgname3) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.$imgname3)) $count ++;
		if(strlen($imgname4) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.$imgname4)) $count ++;
		return $count;
	}


function logadden($text)
{
	$filename = 'logadden.html';
	$somecontent = $text."\n\r";

	// Вначале давайте убедимся, что файл существует и доступен для записи.
	if (is_writable($filename)) {

		// В нашем примере мы открываем $filename в режиме "записи в конец".
		// Таким образом, смещение установлено в конец файла и
		// наш $somecontent допишется в конец при использовании fwrite().
		if (!$fp = fopen($filename, 'a')) {
			echo "Не могу открыть файл ($filename)";
			exit;
		}

		// Записываем $somecontent в наш открытый файл.
		if (fwrite($fp, $somecontent) === FALSE) {
			echo "Не могу произвести запись в файл ($filename)";
			exit;
		}

		fclose($fp);

	} else {
		echo "Файл $filename недоступен для записи";
	}
}

?>
</body>
</head>
</html>