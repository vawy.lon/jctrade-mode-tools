<html prefix="og: http://ogp.me/ns#">
	<head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Автодиас - Фото</title>
        <script type="text/javascript" src="jquery.min.js"></script>
		<script type="text/javascript" src='jquery.cookie.js'></script>
		<script type="text/javascript" src="jquery.vibrate.js"></script>
    </head>
    <body>
		<div id = loopimg>
			<div id = loopclose>Закрыть</div>
			<img id = limage>
		</div>
        <div class = 'content-c'>
        </div>
		<div id = proccessload>50%</div>
		<div id = 'addlist'><a href = 'addjc.php'>добавить всё</a></div>
    </body>
</html>

<style>
	#loopimg.active{
		display: flex;
	}
	#loopimg{
		position: fixed;
		width: 100vw;
		height: 100vh;
		background-color: white;
		z-index: 1;
		display: none;
		justify-content: center;
		align-items: center;
	}
	#loopclose{
		position: absolute;
		right: 37px;
		top: 14px;
		font-size: 22;
		background-color: lightskyblue;
		padding: 8px;
		color: #5f5f5f;
		border-radius: 8px;
	}
	#limage{
		width: 70vw;
		height: calc(70vw * 0.8);
		background-color: #EEE;
		display: flex;
		background-size: contain;
		background-position: center;
		background-repeat: no-repeat;
	}
    body{
        max-width: 100vw;
    }
    body .content-c{
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        align-items: center;
        flex-direction: column-reverse;
        overflow: hidden;
    }
	.input-comment{
		width: 75%;
		min-height: 70px;
		height: auto;
		margin: 5px;
		font-size: 28px;
		background-color: #f7f7f7;
		border: navajowhite;
		text-align: center;
		box-shadow: 0px 0px 5px;
	}
    .block{
        background-color: gainsboro;
        border-radius: 10px;
        overflow: hidden;
        max-width: 370px;
		margin-bottom: 40px;
		transition:0.7s;
    }
    .frontpanel{
        display: grid;
        grid-template-rows: 1fr;
        grid-template-columns: 1fr 1fr 1fr;
    }
    .content-fr{
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
    }
    .content-lr{
        display: flex;
        align-items: center;
    }
    .content-ud{
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
    }
    .benterimg{
        position: relative;
    }
	.lock-text{
		transition-delay: 0.2s;
		transition-duration: 0.2s;
		transition-property: background-color;
		transition-timing-function:  linear;
		color: gray;
		font-size: 1.3em;
		cursor: pointer;
		text-shadow: 0px 0px 0px #0ab970;
	}
	.lock-text.active
	{
		color: green;
		text-shadow: 1px 1px 11px #0ab970;
	}
    .buttondimg{
        background-color: #e0e0e0;
        border: 2px solid #929191;
        border-radius: 4px;
        width: 75px;
        text-align: center;
        text-transform: uppercase;
        margin: 5px;
        height: 25px;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 30px;
        height: 30px;
    }
    .button{
		background-color: #bfbfbf;
    /* border: 2px solid #929191; */
    border-radius: 4px;
    width: 74px;
    text-align: center;
    text-transform: uppercase;
    margin: 5px;
    height: 30px;
    display: flex;
    color: #4a4a4a;
    align-items: center;
    justify-content: center;
    }
    .button:hover{
        background-color: #c5c5c5;
        cursor: pointer;
    }
    .button.active{
        background-color: #abc1f1;
        border: 2px solid #65789b;
    }
    .input-text{
        margin: 5px;
        width: 75%;;
        font-size: 28px;
        background-color: #f7f7f7;
        border: navajowhite;
        text-align: center;
        box-shadow: 0px 0px 5px;
        border-radius: 5px;
    }
    .imagepanel{
        display: flex;
        justify-content: space-between;
        margin-bottom: 30px;
    }
    .img-button{
        width: 80px;
        height: 80px;
        display: flex;
        align-items: center;
        justify-content: center;
        position: relative;
        flex-direction: column;
    }
    .img-button input{
        position: absolute;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
    .buttondloop img:hover{
        opacity: 60%;
        cursor: pointer;
    }
    .buttondloop img{
        width: 15px;
        height: 15px
    }
    .previmg{
        width: 30px;
        height: 30px;
    }
    .buttonpanel{
        display: flex;
        justify-content: center;
		padding: 20px;
    }
	.additem:hover{
		cursor: pointer;
		filter: saturate(0.4);
	}
    .additem{
        background-color: #90aaee;
    	border: 3px solid #004180;
		color: #004180;
        width: 70%;
        font-size: 22px;
        height: 35px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 10px;
        
    }
	.block[handle="add"] .addform .buttonpanel .additem{
		background-color: lightgreen;
        border: 3px solid green;
		color: green;
	}
    .inputpanel{
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-bottom: 5px;
        margin-top: 5px;
    }
	.remove:hover{
		cursor: pointer;
		filter: saturate(0.4);
	}
	.buttondimg:hover
	{
		cursor: pointer;
		filter: opacity(0.4);
	}
	.benterimg:hover
	{
		cursor: pointer;
		filter: opacity(0.4);
	}
    .remove img{
		filter: contrast(0.5);
    }
    .remove{
        width: 30px;
    /* background-color: #808080; */
    display: flex;
    align-items: center;
    height: 30px;
    justify-content: center;
    color: white;
    border-radius: 100%;
    /* box-shadow: 0px 0px 6px black; */
    margin: 10px;
    }
    .inputpanel .podsc{
        overflow: auto;
        display: none;
        position: absolute;
        width: 75%;
        background-color: #f3f3f3;
        left: 13px;
        top: 56px;
        height: 200px;
        z-index: 1;
        border-radius: 10px;
        box-shadow: 0px 0px 10px black;
    }
    .inputpanel .podsc.active{
        display: block;
    }
    .inputpanel .podsc .content{
        padding: 10px
    }
    .inputpanel .podsc .content .items:hover{
        cursor: pointer;
        background-color: cornflowerblue;
    }
    .inputpanel .podsc .content .items
    {
        text-align: center;
        font-size: 12px;
        background-color: white;
        border-radius: 5px;
        box-shadow: 0px 0px 5px;
        margin-bottom: 5px;
        padding: 5px;
    }
    .myitems .item{
        margin: 5px;
        background-color: #ececec;
        padding: 10px;
    }
	.block.line .buttonpanel{
		display: none;
	}
	.block.line .button{
		display: none;
		background: none;
    	border: none;
		width: auto;
	}
	.block.line .button.active{
		display: flex;
	}
	.block.line .buttondimg
	{
		display: none;
	}
	.block.line .remove{
		display: none;
	}
	.block.line .addform{
		display: flex;
		width: 90vw;
		justify-content: space-between;
	}
	.block.line .imagepanel{
		margin-bottom: 0;
	}
	.block.line .img-button{
		width: 45px;
		height: auto;
	}
	.block.line 
	{
		max-width: none;
		margin-bottom: 2px;
		border-radius: none;
		overflow: hidden;
		position: relative;
		height: 55px;
	}
	.block.line .input-text{
		font-size: 12px;
		width: 245px;
		background: none;
		border: none;
		box-shadow: none;
	}
	.block.line .frontpanel{
		width: 140px;
	}
	.block.line .inputpanel{
		position: none;
	}
	.block.line .inputpanel .podsc
	{
		position: fixed;
		width: 95%;
		top: auto;
		left: 0;
	}
	
	.show{
		margin: 10px;
		left: 30px;
		font-size: 22px;
		cursor: pointer;
	}
	.show:hover{
		color: red;
	}
	.cpan{
		display: flex;
		align-items: center;
	}
	.deleteitem, .additemjt{
		background-color: #b9b9b9;
		color: white;
		padding: 5px;
		border-radius: 5px;
		margin-left: 10px;
	}
	.deleteitem:hover,.additemjt:hover{
		cursor: pointer;
		background-color: red;
	}
	#proccessload.active{
		display:block;
	}
	#proccessload{
		position: fixed;
		bottom: 0;
		background-color: #59ca59;
		font-weight: bold;
		color: #396331;
		padding-left: 10px;
		padding-right: 10px;
		padding-top: 3px;
		padding-bottom: 3px;
		margin: 5px;
		display: none;
	}
	
</style>

<script>
	$("html").append('<canvas id ="canvas-resize" style = "display:none;width:900px;height:640px">Обновите браузер</canvas>')
	var proccessload = 0;
	$(document).on('click', '.lock-text', function(){
		var active = $(this).hasClass('active');
		if(active)
		{
			$(this).removeClass('active');
		}
		else
		{
			
			$(this).addClass('active')
		}
	})
    $(document).on('click', '.button', function(){
        var block = $(this).parents('.block').eq(0)
        var front = $(this).attr('front')
		var has = $(this).hasClass('active')
		block.find('.button[front="'+front+'"].active').removeClass('active');
        if(!has)
        {
            $(this).addClass('active');
        }
    })
    $(document).on('click', '.remove', function(){
		var block = $(this).parents('.block').eq(0)
		var it = $(this).attr('it')
		block.find('.input-text[it="'+it+'"]').val('');
    })

    $(document).on('change', ".selectimg", function()
    {
        var block = $(this).parents('.block').eq(0)
        var handle = $(this).attr('handle');

		var reader = new FileReader();
		reader.readAsDataURL(this.files[0]);
		var fil = this.files[0];
		reader.onload = function(e) 
		{

			var img     = new Image();  // Создание нового объекта изображения

			img.src = e.target.result;
			img.onload = function() {
				var example = document.getElementById("canvas-resize");
				example.width = img.naturalWidth/6;
				example.height = img.naturalHeight/6;
				context = example.getContext('2d');
				
				context.drawImage(img, 0, 0, example.width, example.height)  // Рисуем изображение от точки с координатами 0, 0
				var dataImage = example.toDataURL();
				block.find('.previmg[handle="'+handle+'"]').attr('src',dataImage);
			}
			
		}
/*

        var reader = new FileReader();
        reader.onload = function(e) 
        {
            block.find('.previmg[handle="'+handle+'"]').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
        var reader = new FileReader();*/
    });
    $(document).on('click', '.buttondimg', function(){
        var handle = $(this).attr('handle');
        var block = $(this).parents('.block').eq(0)
        removeimg(block, handle)
    })
    function removeimg(block, ind)
    {
		var handle = block.attr('handle');
		$.post('deleteimg.php', {handle: handle, ind: ind}, function(data){
			var arr = JSON.parse(data);
			if(arr['code'] == 1)
			{
				block.find(".selectimg[handle='"+ind+"']").val('')
       	 		block.find(".previmg[handle='"+ind+"']").attr('src', 'addimg.png')
			}
		})
    }
    $(document).on('click', '.additem', function(){
		var handle = $(this).attr('handle');

        var block = $(this).parents('.block').eq(0);
		var handle = block.attr('handle');
		fdata = getData(handle);
		if(handle != 'add')
		{
			fdata.append('handle', handle);
			$.ajax({
				type: 'POST',
				url: 'editimg.php',
				data:  fdata,
				processData: false,
				contentType: false,
				dataType: "text",
				success: function(data) 
				{

					var arr = JSON.parse(data);
					
					if(Number(arr['code']) == 1)
					{
						alert( "Сохранено!" );
					}
					else
					{
						alert( "Ошибка!" );
					}
				},
				error: function(data) {
					alert( "Ошибка!" );
				},
				xhr: function(){
					var xhr = $.ajaxSettings.xhr(); 
					xhr.upload.addEventListener('progress', function(evt){
					if(evt.lengthComputable) { 
						var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
						
						$('#proccessload').html(percentComplete+'%');
						$('#proccessload').addClass('active');
						if(percentComplete >= 100)
						{
							$('#proccessload').removeClass('active')
						}
					}
					}, false);
					return xhr;
				},
				
			});
		}
		if(handle === 'add')
		{
			var modeladd;
			var oemadd;
			var nameadd;
			var commadd;
			var activename = {name: 0, oem: 0, mark: 0, com: 0, col: 0, num: 0, eng: 0};
			if(block.find(".lock-text[it='mark']").hasClass('active'))
			{
				activename['mark'] = 1;
				modeladd = fdata.get('model');
			}
			if(block.find(".lock-text[it='oem']").hasClass('active'))
			{
				activename['oem'] = 1;
				oemadd = fdata.get('oem');
			}
			if(block.find(".lock-text[it='name']").hasClass('active'))
			{
				activename['name'] = 1;
				nameadd = fdata.get('name');
			}
			if(block.find(".lock-text[it='comm']").hasClass('active'))
			{
				activename['com'] = 1;
				commadd = fdata.get('com');
			}
			addblock(
				{
					name: nameadd,
					oem: oemadd,
					model: modeladd,
					comm: commadd,
					handle: 'add'
				}, false, activename
			);
			$.ajax({
				type: 'POST',
				url: 'addimg.php',
				data:  fdata,
				processData: false,
				contentType: false,
				dataType: "text",
				success: function(data) 
				{
					var arr = JSON.parse(data);
					
					if(Number(arr['code']) == -1)
					{
						alert('Не верный формат изображения. Загрузите JPG/JPEG')
					}
					else if(Number(arr['code']) == -2)
					{
						alert('Размер изображения слишком велик!')
					}
					else{
						block.attr('handle', arr['handle']);
						block.find('.show').attr('handle', arr['handle']);
						block.find('.deleteitem').attr('handle', arr['handle']);
						//find('.block').attr('handle', arr['handle'])
					}
				},
				error: function(data) {
				},
				xhr: function(){
					var xhr = $.ajaxSettings.xhr(); 
					xhr.upload.addEventListener('progress', function(evt){
					if(evt.lengthComputable) { 
						var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
						
						$('#proccessload').html(percentComplete+'%');
						$('#proccessload').addClass('active');
						if(percentComplete >= 100)
						{
							$('#proccessload').removeClass('active')
						}
					}
					}, false);
					return xhr;
				},
			});
		}
    })
    function additem(items)
    {
        $.each(items, function(i, item){
            var fr = '';
            if(item['fr'] == 'F') fr = 'передняя';
            else if(item['fr'] == 'R') fr = 'задняя';
            else fr = '';

            var lr = '';
            if(item['lr'] == 'L') lr = 'левая';
            else if(item['lr'] == 'R') lr = 'правая';
            else lr = '';

            var ud = '';
            if(item['ud'] == 'U') ud = 'верхняя';
            else if(item['ud'] == 'D') ud = 'нижняя';
            else ud = '';
            $('.myitems').prepend("\
                        <div class = item>\
                            <div class = name>\
                            Название: "+item['name']+"<br>\
                            Марка: "+item['model']+"<br>\
                            Сторона: "+fr+" "+lr+" "+ ud +"\
                            </div>\
                       </div>")
        })
    }
    function getData(handle)
    {
        handle = $('.block[handle='+handle+']');
        var data = new FormData();
        data.append('name', handle.find('.input-text[it="name"]').val());
        data.append('model', handle.find('.input-text[it="model"]').val());
		data.append('oem', handle.find('.input-text[it="oem"]').val());
		data.append('comm', handle.find('.input-text[it="comm"]').val());
		data.append('color', handle.find('.input-text[it="color"]').val());
		data.append('num', handle.find('.input-text[it="num"]').val());
		data.append('eng', handle.find('.input-text[it="eng"]').val());

        if(handle.find('.button#front').hasClass('active')) data.append('fr', 'F');
        else if(handle.find('.button#back').hasClass('active')) data.append('fr', 'R');
        else data.append('fr', '');

        if(handle.find('.button#left').hasClass('active')) data.append('lr', 'L');
        else if(handle.find('.button#right').hasClass('active')) data.append('lr', 'R');
        else data.append('lr', '');

        if(handle.find('.button#up').hasClass('active')) 
		{
			data.append('ud', 'U');
		}
        else if(handle.find('.button#down').hasClass('active')) 
		{
			data.append('ud', 'D');
		}
        else 
		{
			data.append('ud', '');
		}

        if(handle.find(".previmg[handle='1']").attr('src').length > 100) {
			data.append('file_1', handle.find(".previmg[handle='1']").attr('src'))
			console.log('append img 1');
			handle.find('.selectimg[handle="1"]').val(''); 

		}
        if(handle.find(".previmg[handle='2']").attr('src').length > 100) {
			data.append('file_2', handle.find(".previmg[handle='2']").attr('src'))
			handle.find('.selectimg[handle="2"]').val(''); 
		}
        if(handle.find(".previmg[handle='3']").attr('src').length > 100) {
			data.append('file_3', handle.find(".previmg[handle='3']").attr('src'))
			handle.find('.selectimg[handle="3"]').val(''); 
		}
        if(handle.find(".previmg[handle='4']").attr('src').length > 100) {
			data.append('file_4', handle.find(".previmg[handle='4']").attr('src'))
			handle.find('.selectimg[handle="4"]').val(''); 
		}
        return data;
    }
    $(document).on('focusout', '.input-text', function(){

        var int = $(this).siblings(".podsc");
		$.cookie($(this).attr('it'), $(this).val());
        setTimeout(function(){
            int.removeClass('active');
        }, 300);
    })
    $(document).on('focusin', '.input-text', function(){
        $(this).siblings(".podsc").addClass('active');
    })
    $(document).on('click', '.items', function(){
        var handle = $(this).parents('.block').eq(0);
        var it = $(this).attr('it')
        handle.find(".input-text[it='"+it+"']").val($(this).html());
    })
	
    $(document).on('input', '.input-text[it=name]', function(){
        
        $.get('getnames.php', {'name': $(this).val()}, function(data){
            var content = $(".podsc.name .content");
            content.html("");
            var arr = JSON.parse(data);
            $.each(arr, function(i, v){
                content.append("<div class = 'items name' it = 'name'>"+v['n']+"</div>")
            })
        })
    })

    $(document).on('input', '.input-text[it=model]', function(){
        
        var line = $(this).val()
        var arg = line.split(' ')
        var args = arg.length;
        var mark = '';
        var model = '';
        var kuz = '';

        if(args == 1) kuz = arg[0];
        if(args == 2) 
        {
            mark = arg[0];
            model = arg[1];
        }
        if(args == 3) 
        {
            mark = arg[0];
            model = arg[1];
            kuz = arg[2];
        }
        $.post('serchmodel.php', {mark: mark, model: model, kuzov: kuz, args: args}, function(data){
            var arr = JSON.parse(data);
            var content = $(".podsc.model .content");
            content.html("");
            var arr = JSON.parse(data);
            $.each(arr, function(i, v){
                content.append("<div class = 'items' it = 'model'>"+v['mark']+" "+v['model']+" "+v['kuzov']+"</div>")
            })
        })
    })
	//first — первая функция,которую нужно запустить
	wait = function(first){ 
       	//класс для реализации вызова методов по цепочке
	return new (function(){ 
		var self = this;
		var callback = function(){
			var args;
			if(self.deferred.length) {
				/* превращаем массив аргументов
 				  в обычный массив */
				args = [].slice.call(arguments); 

				/* делаем первым аргументом функции-обертки
 				  коллбек вызова следующей функции */
				args.unshift(callback); 

				//вызываем первую функцию в стеке функций
				self.deferred[0].apply(self, args); 

				//удаляем запущенную функцию из стека
				self.deferred.shift(); 
			}
		}
		this.deferred = []; //инициализируем стек вызываемых функций

		this.wait = function(run){
			//добавляем в стек запуска новую функцию
			this.deferred.push(run); 

			//возвращаем this для вызова методов по цепочке
			return self; 
		}

		first(callback); //запуск первой функции
	});
}	
    $(document).ready(function(){
		loadblocks()
		setTimeout(function(){
			addblock({handle: 'add'}, false);
		}, 300)
		
		
    })


    function loadblocks()
    {
        $.post('loadadden.php', function(data){
            var arr = JSON.parse(data);
            var html = '';
            $.each(arr, function(i, v){
                var frF = '';
                var frR = '';

                var lrL = '';
                var lrR = '';
                
                var udU = '';
                var udD = '';

                if(v['fr'] == 'F') frF = 'active'; else if(v['fr'] == 'R') frR = 'active';
                if(v['lr'] == 'L') lrL = 'active'; else if(v['lr'] == 'R') lrR = 'active';
                if(v['ud'] == 'U') udU = 'active'; else if(v['ud'] == 'D') udD = 'active';
				data = {
					'name': v['name'],
					'handle': v['handle'],
					'mark': v['mark'],
					'model': v['model'],
					'kuzov': v['kuzov'],
					'color': v['color'],
					'num': v['num'],
					'oem': v['oem'],
					'comm': v['comm'],
					'img_1': v['image1'],
					'img_2': v['image2'],
					'img_3': v['image3'],
					'img_4': v['image4'],
					'frF': frF,
					'frR': frR,
					'lrL': lrL,
					'lrR': lrR,
					'udD': udD,
					'udU': udU
				};
                addblock(data, false);
            })
        })
    }
    function addblock(data, end = false, active)
    {
		var button = '';
		
		if(typeof active === 'undefined')
		{
			active = {name: 0, oem: 0, mark: 0, com: 0, col: 0, num: 0, eng: 0};
		}
		var activeclass = {};
		if( active['name'] == 0)  activeclass['name'] = ''; else activeclass['name'] = 'active';
		if( active['oem'] == 0)  activeclass['oem'] = ''; else activeclass['oem'] = 'active';
		if( active['mark'] == 0)  activeclass['mark'] = ''; else activeclass['mark'] = 'active';
		if( active['com'] == 0)  activeclass['com'] = ''; else activeclass['com'] = 'active';
		if( active['col'] == 0)  activeclass['col'] = ''; else activeclass['col'] = 'active';
		if( active['num'] == 0)  activeclass['num'] = ''; else activeclass['num'] = 'active';
		if( active['eng'] == 0)  activeclass['eng'] = ''; else activeclass['eng'] = 'active';

		var namemodel = '';
		if(typeof data['handle'] === 'undefined')  data['handle'] = 'add';
		if(typeof data['name'] === 'undefined')  data['name'] = '';
		if(typeof data['frF'] === 'undefined')  data['frF'] = '';
		if(typeof data['frR'] === 'undefined')  data['frR'] = '';
		if(typeof data['lrL'] === 'undefined')  data['lrL'] = '';
		if(typeof data['lrR'] === 'undefined')  data['lrR'] = '';
		if(typeof data['udD'] === 'undefined')  data['udD'] = '';
		if(typeof data['udU'] === 'undefined')  data['udU'] = '';
		if(typeof data['mark'] === 'undefined')  data['mark'] = '-';
		if(typeof data['model'] === 'undefined')  data['model'] = '-';
		if(typeof data['kuzov'] === 'undefined')  data['kuzov'] = '-';
		if(typeof data['oem'] === 'undefined')  data['oem'] = '';
		if(typeof data['comm'] === 'undefined')  data['comm'] = '';
		if(typeof data['color'] === 'undefined')  data['color'] = '';
		if(typeof data['num'] === 'undefined')  data['num'] = '';

		if (data['handle'] === 'add') 
		{
			if(data['model'].length > 1 ) 
			{
				namemodel = data['model'];
			}
		}
		else
		{
			namemodel = data['mark'] + ' '+ data['model'] + ' '+ data['kuzov'];
		}

		if(typeof data['img_1'] === 'undefined' || data['img_1'].length == 0)  data['img_1'] = 'addimg.png'; else data['img_1'] = '../images/'+data['img_1'];
		if(typeof data['img_2'] === 'undefined' || data['img_2'].length == 0)  data['img_2'] = 'addimg.png'; else data['img_2'] = '../images/'+data['img_2'];
		if(typeof data['img_3'] === 'undefined' || data['img_3'].length == 0)  data['img_3'] = 'addimg.png'; else data['img_3'] = '../images/'+data['img_3'];
		if(typeof data['img_4'] === 'undefined' || data['img_4'].length == 0)  data['img_4'] = 'addimg.png'; else data['img_4'] = '../images/'+data['img_4'];
		if(data['handle'] == 'add') button = 'Добавить'; else button = 'Изменить'; 
		var blclass = '';
		if(data['handle'] != 'add') blclass = 'edit';
		var html = `
		<div class="block `+blclass+`" handle = '`+data['handle']+`'>
				<div class = 'addform'>
					<div class = inputpanel>
					<div class = 'lock-text `+activeclass['name']+`' it = 'name'>◉︎</div>
						<input type = text class = 'input-text' it = 'name' value = "`+data['name']+`" placeholder = 'название'>
						<div class = "podsc name">
							<div class = 'content'></div>
						</div>
					<div class = 'remove'  it = 'name'><img src="remove.png" width="20" height="20"></div>
				</div>
				<div class = 'addform'>
					<div class = inputpanel>
					<div class = 'lock-text `+activeclass['oem']+`' it = 'oem'>◉︎</div>
					<input type = text class = 'input-text' it = 'oem' value = "`+data['oem']+`" placeholder = 'oem код'>
					<div class = 'remove'  it = 'oem'><img src="remove.png" width="20" height="20"></div>
				</div>
				<div class = frontpanel>
					<div class = content-fr>
						<div class = 'button `+data['frF']+`' front = 'fr' id = front>Перед</div>
						<div class = 'button `+data['frR']+`' front = 'fr' id = back>Зад</div>
					</div>
					<div class = content-lr>
						<div class = 'button  `+data['lrL']+`' front = 'lr' id = left>Лево</div>
						<div class = 'button  `+data['lrR']+`' front = 'lr' id = right>Право</div>
					</div>
					<div class = content-ud>
						<div class = 'button  `+data['udD']+`' front = 'ud' id = up>Верх</div>
						<div class = 'button  `+data['udU']+`' front = 'ud' id = down>Низ</div>
					</div>
			</div>
			<div class = inputpanel>
					<div class = 'lock-text `+activeclass['mark']+`' it = 'mark'>◉︎</div>
					<input type = text class = 'input-text' it = 'model' value = "`+namemodel+`" placeholder = 'Марка'>
					<div class = "podsc model">
					<div class = 'content'>
					</div>
				</div>
				<div class = 'remove'  it = 'model'><img src="remove.png" width="20" height="20"></div>
			</div>
			<div class = imagepanel>

					<div class = img-button>
						<div class = 'buttondimg' handle = 1><img src="remove.png" width="20" height="20"></div>
						<div class = benterimg>
							<input value = '' class = selectimg handle = 1 type = file>
							<img class = 'previmg' handle = 1 src="`+data['img_1']+`">
						</div>
						<div class = 'buttondloop' handle = 1><img src="lup.png"></div>
					</div>
					<div class = img-button>
						<div class = 'buttondimg' handle = 2><img src="remove.png" width="20" height="20"></div>
						<div class = benterimg>
							<input value = '' class = selectimg handle = 2 type = file>
							<img class = 'previmg' handle = 2 src="`+data['img_2']+`">
						</div>
						<div class = 'buttondloop' handle = 1><img src="lup.png"></div>
					</div>

					<div class = img-button>
						<div class = 'buttondimg' handle = 3><img src="remove.png" width="20" height="20"></div>
						<div class = benterimg>
							<input value = '' class = selectimg handle = 3 type = file>
							<img class = 'previmg' handle = 3 src="`+data['img_3']+`">
						</div>
						<div class = 'buttondloop' handle = 1><img src="lup.png"></div>
					</div>

					<div class = img-button>
						<div class = 'buttondimg' handle = 4><img src="remove.png" width="20" height="20"></div>
						<div class = benterimg>
							<input value = '' class = selectimg handle = 4 type = file>
							<img class = 'previmg' handle = 4 src="`+data['img_4']+`">
						</div>
						<div class = 'buttondloop' handle = 1><img src="lup.png"></div>
					</div>
				</div>
				<div class = 'aparams'>
					<div class = 'addform'>
						<div class = inputpanel>
						<div class = 'lock-text `+activeclass['com']+`' it = 'com'>◉︎</div>
						<input class = 'input-text' it = 'comm'  value = '`+data['comm']+`'placeholder = 'Коментарий'>
						<div class = 'remove'  it = 'comm'><img src="remove.png" width="20" height="20"></div>
					</div>
					<div class = 'addform'>
						<div class = inputpanel>
						<div class = 'lock-text `+activeclass['col']+`' it = 'col'>◉︎</div>
						<input class = 'input-text' it = 'color'  value = '`+data['color']+`'placeholder = 'Цвет'>
						<div class = 'remove'  it = 'color'><img src="remove.png" width="20" height="20"></div>
					</div>
					<div class = 'addform'>
						<div class = inputpanel>
						<div class = 'lock-text `+activeclass['номер']+`' it = 'num'>◉︎</div>
						<input class = 'input-text' it = 'num'  value = '`+data['num']+`'placeholder = 'Номер'>
						<div class = 'remove'  it = 'num'><img src="remove.png" width="20" height="20"></div>
					</div>
					<div class = 'addform'>
						<div class = inputpanel>
						<div class = 'lock-text `+activeclass['eng']+`' it = 'eng'>◉︎</div>
						<input class = 'input-text' it = 'eng'  value = '`+data['num']+`'placeholder = 'Двигатель'>
						<div class = 'remove'  it = 'eng'><img src="remove.png" width="20" height="20"></div>
					</div>
				</div>
				<div class = buttonpanel>
					<div class = 'additem'>`+button+`</div>
				</div>
				<div class = cpan>
					<div class = 'deleteitem' handle = '`+data['handle']+`'>Удалить</div>
				</div>
			</div>
		</div>`;
        if(end == true) 
		{
			$('.content-c').prepend(html);
		}
		else 
		{
			$('.content-c').append(html);
		}
    }

	$(document).on('click', '#modeline', function(){
		$('.block').addClass('line');
		$('.block[handle="add"]').removeClass('line');
	})

	$(document).on('click', '#modebloks', function(){
		$('.block').removeClass('line');
	})

	$(document).on('click', '.buttondloop', function(){
		var imgsrc = $(this).siblings('.benterimg').find('.previmg').attr('src');
		$('#loopimg').addClass('active');
		$('#limage').attr("src", imgsrc);
	})

	$(document).on('click', '#loopclose', function(){
		$('#loopimg').removeClass('active');
	})

	$(document).on('click', '.show', function(){
		
		$('.block[handle = "'+$(this).attr('handle')+'"]').toggleClass('line');
	})

	$(document).on('click', '.deleteitem', function(){
		var res = prompt("Удалить?\nВведите: 1");
		var handle = $(this).attr('handle');
		if(res == '1')
		{
			$.post('deleteitem.php', {handle: handle}, function(data){
				var arr = JSON.parse(data);
				if(arr['code'] == 1)
				{
					alert("Строка удалена!");
					$('.block[handle="'+handle+'"]').remove();
				}
				else
				{
					alert("Ошибка!");
				}
			})
		}
	})
</script>