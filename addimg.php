<?php
	$file1 = "";
	if(isset($_POST['file_1'])) $file1 = $_POST['file_1'];
	$file2 = "";
	if(isset($_POST['file_2'])) $file2 = $_POST['file_2'];
	$file3 = "";
	if(isset($_POST['file_3'])) $file3 = $_POST['file_3'];
	$file4 = "";
	if(isset($_POST['file_4'])) $file4 = $_POST['file_4'];

    $data = saveLine(
            $_POST['name'], 
            $_POST['fr'], 
            $_POST['lr'], 
            $_POST['ud'], 
            $_POST['model'], 
            $_POST['oem'], 
            $file1,
            $file2,
            $file3,
            $file4,
			$_POST['comm'],
			$_POST['color'],
			$_POST['num'],
			$_POST['eng']
        );
    echo json_encode($data);
    function saveLine($name, $fr, $lr, $ud, $model, $ind, $file1, $file2, $file3, $file4, $comm, $color, $num, $eng)
    {
            include_once("data.php");
            $images = saveImages($file1, $file2, $file3, $file4);

            $line = array();
            $arg = array();
            $arg = explode(" ", $model);
			if(!isset($arg[0])) $arg[0] = '';
			if(!isset($arg[1])) $arg[1] = '';
			if(!isset($arg[2])) $arg[2] = '';
            $line['name'] = $name;
            $line['handle'] = RandomString();
            $line['mark'] = $arg[0];
            $line['model'] = $arg[1];
            $line['kuzov'] = $arg[2];
            $line['fr'] = $fr;
            $line['lr'] = $lr;
            $line['ud'] = $ud;
			$line['oem'] = $ind;
			$line['comm'] = $comm;
			$line['color'] = $color;
			$line['num'] = $num;
			$line['eng'] = $num;
            $line['image1'] = $images[0];
            $line['image2'] = $images[1];
            $line['image3'] = $images[2];
            $line['image4'] = $images[3];
            adddata($line);
        return $line;
    }
    function saveImages($file1, $file2, $file3, $file4)
    {

        return array(
			saveFileUpdate($file1), 
			saveFileUpdate($file2), 
			saveFileUpdate($file3), 
			saveFileUpdate($file4)
		);
    }
    function RandomString($size = 16)
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        // Output: 54esmdr0qf
        return substr(str_shuffle($permitted_chars), 0, $size);
    }

	function saveFileUpdate($file)
	{
		if(strlen( $file) < 10) {
			return ""; 
		}
		$name = RandomString().'.jpg';
		$img = str_replace('data:image/png;base64,', '', $file);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		file_put_contents( $_SERVER['DOCUMENT_ROOT']."/images/".$name, $data);
		return $name;
	}
?>