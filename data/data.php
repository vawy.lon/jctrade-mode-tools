<?php

    function settings_get($line = NULL)
    {
        if(!file_exists("settings.json"))
        {
            $f = fopen("settings.json", 'w');
            fclose($f);
        }
        $text = file_get_contents("settings.json");
        $arr = json_decode($text, true);
        if($line == NULL) return $arr;
        return isset($arr[$line])?$arr[$line]:NULL;
    }
    function settings_set($line, $data)
    {
        $arr = settings_get();
        $arr[$line] = $data;
        $text = json_encode($arr, JSON_NUMERIC_CHECK);
        $f = fopen("settings.json", 'w');
        $f = fwrite($f, $text);
    }
    function createdata()
    {
        $data = array();
        if(!file_exists('data.json'))
        {
            $f = fopen('data.json', 'w');
            fwrite($f, json_encode($data));
        }
    }
    function loaddata()
    {
        createdata();
        $text = file_get_contents('data.json');
        $data = json_decode($text, true);
        return $data;
    }
    function savedata($data)
    {
        createdata();
        $f = fopen('data.json', 'w');
        fwrite($f, json_encode($data));
        fclose($f);
    }
	function setdata($handle, $arg, $val)
	{
		$data = loaddata();
		$i = 0;
		foreach ($data as $line)
		{
			//echo var_dump($line['image1']);
			if($data[$i]['handle'] == $handle)
			{
				$data[$i][$arg] = $val;
				savedata($data);
				return 1;
			}
			$i ++;
		}
		return 0;
	}
	function deleteline($handle)
	{
		$data = loaddata();
		$newdata = array();
		$ret = 0;
		$i = 0;

		foreach ($data as $line)
		{
			//echo var_dump($line['image1']);
			if($line['handle'] != $handle)
			{
				array_push($newdata, $line);
			}
			else $ret = 1;
			$i ++;
		}
		savedata($newdata);
		return $ret;
	}
	function editdata($handle, $val = array())
	{
		$data = loaddata();
		$i = 0;
		foreach ($data as $line)
		{
			//echo var_dump($line['image1']);
			if($data[$i]['handle'] == $handle)
			{
				$data[$i] = $val;
				savedata($data);
				return 1;
			}
			$i ++;
		}
		return 0;
	}
    function adddata($line)
    {
        $data = loaddata();
        array_push($data, $line);
        savedata($data);
	}

?>