<?php

	$file1 = "";
	if(isset($_POST['file_1'])) $file1 = $_POST['file_1'];
	$file2 = "";
	if(isset($_POST['file_2'])) $file2 = $_POST['file_2'];
	$file3 = "";
	if(isset($_POST['file_3'])) $file3 = $_POST['file_3'];
	$file4 = "";
	if(isset($_POST['file_4'])) $file4 = $_POST['file_4'];

    $data = editLine(
			$_POST['handle'],
            $_POST['name'], 
            $_POST['fr'], 
            $_POST['lr'], 
            $_POST['ud'], 
            $_POST['model'], 
            $_POST['oem'], 
            $file1,
            $file2,
            $file3,
            $file4,
			$_POST['comm'],
			$_POST['color'],
			$_POST['num'],
			$_POST['eng']
        );
	$data['code'] = 1;
	include_once("data.php");
	function loaddataline($handle)
    {
        $data = loaddata();
		$i = 0;
		foreach ($data as $line)
		{
			if($data[$i]['handle'] == $handle)
			{
				return $data[$i];
			}
			$i ++;
		}
		return array();
    }
    function editLine($handle, $name, $fr, $lr, $ud, $model, $ind, $file1, $file2, $file3, $file4, $comm, $color, $num, $eng)
    {
            include_once("data.php");

            $images = saveImages($file1, $file2, $file3, $file4);
			$olddata = loaddataline($handle);

            $line = array();
            $arg = array();
            $arg = explode(" ", $model);
			if(!isset($arg[0])) $arg[0] = '';
			if(!isset($arg[1])) $arg[1] = '';
			if(!isset($arg[2])) $arg[2] = '';
            $line['name'] = $name;
            $line['handle'] = $handle;
            $line['mark'] = $arg[0];
            $line['model'] = $arg[1];
            $line['kuzov'] = $arg[2];
            $line['fr'] = $fr;
            $line['lr'] = $lr;
            $line['ud'] = $ud;
			$line['oem'] = $ind;
			$line['comm'] = $comm;
			$line['color'] = $color;
			$line['num'] = $num;
			$line['eng'] = $eng;

			$filename = saveFileUpdate($file1);
			if(strlen($filename)  > 0) $line['image1'] = $filename;
			else if(isset($olddata['image1'])) $line['image1'] = $olddata['image1'];

			$filename = saveFileUpdate($file2);
			if(strlen($filename)  > 0) $line['image2'] = $filename;
			else if(isset($olddata['image2'])) $line['image2'] = $olddata['image2'];

			$filename = saveFileUpdate($file3);
			if(strlen($filename)  > 0) $line['image3'] = $filename;
			else if(isset($olddata['image3'])) $line['image3'] = $olddata['image3'];

			$filename = saveFileUpdate($file4);
			if(strlen($filename)  > 0) $line['image4'] = $filename;
			else if(isset($olddata['image4'])) $line['image4'] = $olddata['image4'];

            //$line['image1'] = $images[0];
           // $line['image2'] = $images[1];
           // $line['image3'] = $images[2];
            //$line['image4'] = $images[3];
            editdata($handle, $line);
        return $line;
    }
    function saveImages($file1, $file2, $file3, $file4)
    {

        return array(
			saveFileUpdate($file1), 
			saveFileUpdate($file2), 
			saveFileUpdate($file3), 
			saveFileUpdate($file4)
		);
    }
    function RandomString($size = 16)
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        // Output: 54esmdr0qf
        return substr(str_shuffle($permitted_chars), 0, $size);
    }

	function resize($image, $w_o = false, $h_o = false) {
		if (($w_o < 0) || ($h_o < 0)) {
		  return false;
		}
		list($w_i, $h_i, $type) = getimagesize($image); 
		$types = array("", "gif", "jpeg", "png");
		$ext = $types[$type]; //  ""  ,   
		if ($ext) {
		  $func = 'imagecreatefrom'.$ext; //   ,  ,   
		  $img_i = $func($image); //       
		} else {
		  return false;
		}
		/*    1 ,     */
		if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
		if (!$w_o) $w_o = $h_o / ($h_i / $w_i);
		$img_o = imagecreatetruecolor($w_o, $h_o); //     
		imagecopyresampled($img_o, $img_i, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i); //      ,  
		$func = 'image'.$ext; //     
		return $func($img_o, $image); //      ,   ,    
	  }
	  /*          100 ,    ,     */


	  function saveFileUpdate($file)
	{
		if(strlen( $file) < 10) {
			return ""; 
		}
		$name = RandomString().'.jpg';
		$img = str_replace('data:image/png;base64,', '', $file);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		file_put_contents( $_SERVER['DOCUMENT_ROOT']."/images/".$name, $data);
		return $name;
	}
?>