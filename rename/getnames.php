<?php
    require('../db.php');
    $query = "SELECT 
    `barcode`.`BARCODE` as `barcode`,
    `category`.`NAME` as `name`,
    `barcode`.`FIELD1` as `mark`,
    `barcode`.`FIELD2` as `model`,
    `barcode`.`FIELD3` as `kuzov`,
    `barcode`.`FIELD4` as 'rename',
    `barcode`.`FIELD5` as 'engine',
    `barcode`.`FIELD6` as 'FR',
    `barcode`.`FIELD7` as 'LR',
    `barcode`.`FIELD8` as 'UD',
    `barcode`.`FIELD11` as 'oem',
    `barcode`.`FIELD12` as 'color',
    `barcode`.`FIELD13` as 'god',
    `barcode`.`COST_RUB` as 'price',
    `barcode`.`HAVE_PHOTO` as 'isimg',
    `barcode`.`DATE_INS` as 'ins',
    `barcode`.`DATE_UPD` as 'upd'
    FROM `barcode` LEFT JOIN `category` ON `barcode`.`CATEGORY` = `category`.`CATEGORY` WHERE `IS_ENABLE` = '1'";
    $res = mysqli_query($mysql, $query);
    if($res)
    {
        $r=0;
        $dat = [
                    'data'=>[
                        'name'=>[],
                        'mark'=>[],
                        'model'=>[],
                        'kuzov'=>[],
                        'rename'=>[],
                        'engine'=>[],
                        'FR'=>[],
                        'LR'=>[],
                        'UD'=>[],
                        'oem'=>[],
                        'color'=>[],
                        'god'=>[],
                        'price'=>[],
                        'isimg'=>[],
                        'ins'=>[],
                        'upd'=>[]
                    ],
                    'group'=>[
                        'name'=>[],
                        'mark'=>[],
                        'model'=>[],
                        'kuzov'=>[],
                        'rename'=>[],
                        'engine'=>[],
                        'FR'=>[],
                        'LR'=>[],
                        'UD'=>[],
                        'oem'=>[],
                        'color'=>[],
                        'god'=>[],
                        'price'=>[],
                        'isimg'=>[],
                        'ins'=>[],
                        'upd'=>[]
                    ]
        ];
        while($row = mysqli_fetch_assoc($res)){
            array_push($dat['data']['name'], $row['name']);
            array_push($dat['data']['mark'], $row['mark']);
            array_push($dat['data']['model'], $row['model']);
            array_push($dat['data']['kuzov'], $row['kuzov']);
            array_push($dat['data']['rename'], $row['rename']);
            array_push($dat['data']['engine'], $row['engine']);
            array_push($dat['data']['FR'], $row['FR']);
            array_push($dat['data']['LR'], $row['LR']);
            array_push($dat['data']['UD'], $row['UD']);
            array_push($dat['data']['oem'], $row['oem']);
            array_push($dat['data']['color'], $row['color']);
            array_push($dat['data']['god'], $row['god']);
            array_push($dat['data']['price'], $row['price']);
            array_push($dat['data']['isimg'], $row['isimg']);
            array_push($dat['data']['ins'], $row['ins']);
            array_push($dat['data']['upd'], $row['upd']);
        }
        array_push($dat['group']['name'],   array_unique( $dat['data']['name'], SORT_STRING ));
        array_push($dat['group']['mark'],   array_unique( $dat['data']['mark'], SORT_STRING ));
        array_push($dat['group']['model'],  array_unique( $dat['data']['model'], SORT_STRING ));
        array_push($dat['group']['kuzov'],  array_unique( $dat['data']['kuzov'], SORT_STRING ));
        array_push($dat['group']['rename'], array_unique( $dat['data']['rename'], SORT_STRING ));
        array_push($dat['group']['engine'], array_unique( $dat['data']['engine'], SORT_STRING ));
        array_push($dat['group']['FR'],     array_unique( $dat['data']['FR'], SORT_STRING ));
        array_push($dat['group']['LR'],     array_unique( $dat['data']['LR'], SORT_STRING ));
        array_push($dat['group']['UD'],     array_unique( $dat['data']['UD'], SORT_STRING ));
        array_push($dat['group']['oem'],    array_unique( $dat['data']['oem'], SORT_STRING ));
        array_push($dat['group']['color'],  array_unique( $dat['data']['color'], SORT_STRING ));
        array_push($dat['group']['god'],    array_unique( $dat['data']['god'], SORT_STRING ));
        array_push($dat['group']['price'],  array_unique( $dat['data']['price'], SORT_STRING ));
        array_push($dat['group']['isimg'],  array_unique( $dat['data']['isimg'], SORT_STRING ));
        array_push($dat['group']['ins'],    array_unique( $dat['data']['ins'], SORT_STRING ));
        array_push($dat['group']['upd'],    array_unique( $dat['data']['upd'], SORT_STRING ));
        //var_dump($dat['name']);
        echo json_encode($dat, JSON_NUMERIC_CHECK);
    }
    else
    {
        echo mysqli_error($mysql);
    }
    
?>