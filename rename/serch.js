var m_data = [];
var m_serch = [];
$(document).on('blur', 'input', function(){
    var elem = $(this).parents('.serchcontent').eq(0)
    var rt = elem.find('.remark');
    setTimeout(function(){
        rt.removeClass('active')
    }, 200);
});

$(document).on('focus', 'input', function(){
    $(this).val('')
    var elem = $(this).parents('.serchcontent').eq(0)
    var rt = elem.find('.remark')
    rt.addClass('active')
    appendRes($(this).attr('row'), $(this).val())
});



load_names();
function load_names()
{
    $.ajax({
        type: 'POST',
        url: 'getnames.php',
        processData: false,
        contentType: false,
        dataType: "text",
        success: function(data) 
        {
                m_data = JSON.parse(data);
        },
        error: function(data) {
            alert( "Ошибка!" );
        },
    });
}

function DrawResults()
{
    var serch = getserch();
    var result = [];
    var rows ={name:[], mark:[], model:[], kuzov:[], FR:[], LR:[], UD:[], engine:[]}
    $.each(m_data['data']['name'], function(i, v){
            result.push(i);
    })
    if(serch['name'].length > 0)    $.each(result, function(i, v){if(m_data['data']['name'][v] !== serch['name'])      {result[i] = -1;}})
    if(serch['mark'].length > 0)    $.each(result, function(i, v){if(m_data['data']['mark'][v] !== serch['mark'])      {result[i] = -1;}})
    if(serch['model'].length > 0)   $.each(result, function(i, v){if(m_data['data']['model'][v] !== serch['model'])    {result[i] = -1;}})
    if(serch['kuzov'].length > 0)   $.each(result, function(i, v){if(m_data['data']['kuzov'][v] !== serch['kuzov'])    {result[i] = -1;}})
    if(serch['FR'].length > 0)      $.each(result, function(i, v){if(m_data['data']['FR'][v] !== serch['FR'])          {result[i] = -1;}})
    if(serch['LR'].length > 0)      $.each(result, function(i, v){if(m_data['data']['LR'][v] !== serch['LR'])          {result[i] = -1;}})
    if(serch['UD'].length > 0)      $.each(result, function(i, v){if(m_data['data']['UD'][v] !== serch['UD'])          {result[i] = -1;}})
    if(serch['engine'].length > 0)  $.each(result, function(i, v){if(m_data['data']['engine'][v] !== serch['engine'])  {result[i] = -1;}})

    $.each(result, function(i, v){
        if(result[i] != -1)
        {
        rows["name"].push(m_data['data']['name'][v])
        rows["mark"].push(m_data['data']['mark'][v])
        rows["model"].push(m_data['data']['model'][v])
        rows["kuzov"].push(m_data['data']['model'][v])
        rows["FR"].push(m_data['data']['FR'][v])
        rows["LR"].push(m_data['data']['LR'][v])
        rows["UD"].push(m_data['data']['UD'][v])
        rows["engine"].push(m_data['data']['engine'][v])
        }
    });
    appendResultSerch(rows)
}
$(document).on('click', '#buttonserch', function()
{
    DrawResults();
   // console.log(rows);
})

function appendResultSerch(rows)
{
    var text = "";
    $.each(rows["name"], function(i, v){
        text = text+"<div class = 'lineserch'>";
        text = text+"<div>"+rows["name"][i]+"</div>"
        text = text+"<div>"+rows["mark"][i]+"</div>"
        text = text+"<div>"+rows["model"][i]+"</div>"
        text = text+"<div>"+rows["FR"][i]+"</div>"
        text = text+"<div>"+rows["LR"][i]+"</div>"
        text = text+"<div>"+rows["UD"][i]+"</div>"
        text = text+"<div>"+rows["engine"][i]+"</div>"
        text = text+"</div>";
    });
    $('#resultserch').html(text);

}

$(document).on('click', '.rowselected', function(){
	$('input[row="'+$(this).attr('row')+'"').val($(this).html())
    DrawResults();
})

$(document).on('input', 'input', function(){
    appendRes($(this).attr('row'), $(this).val())
})

function serchGroup(row, serch)
{
    var vanesgroup = [];
    var serch = getserch();
	$.each(m_data['group'][row][0], function(index, value)
   	{
		if(typeof value === 'number') 
		{
				console.log(String(value) )
		}
		if(String(value).toLowerCase().includes(String(serch).toLowerCase()))
		{
			vanesgroup.push(m_data['group'][row][0][index])
		}
		//	return vanesgroup;
   	});
	
	console.log(vanesgroup);
    return vanesgroup;
}
function appendRes(row, serch)
{
    var remark = $('.remark[row='+row+']');
    remark.html('')
    var text = '';
    $.each(serchGroup(row, serch), function(index, value){
        text = text+'<div class = "rowselected" row = "'+row+'">'+value+'</div>';
    });
    remark.html(text);
}

function getserch()
{
    m_serch['name'] = $('input[row="name"').val();
    m_serch['mark'] = $('input[row="mark"').val();
    m_serch['model'] = $('input[row="model"').val();
    m_serch['kuzov'] = $('input[row="kuzov"').val();
    m_serch['FR'] = $('input[row="FR"').val();
    m_serch['LR'] = $('input[row="LR"').val();
    m_serch['UD'] = $('input[row="UD"').val();
    m_serch['engine'] = $('input[row="engine"').val();
    return m_serch;
}
