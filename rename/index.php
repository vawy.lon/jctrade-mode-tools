<!DOCTYPE html>
    <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Автодиас - Фото</title>
            <script type="text/javascript" src="../jquery.min.js"></script>
            <script type="text/javascript" src='../jquery.cookie.js'></script>
            <script type="text/javascript" src="../jquery.vibrate.js"></script>
            <script type="text/javascript" src="serch.js"></script>
            <link href="styles.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class = "head">
        <div class = "button-serch" id = "buttonserch">
                Искать
            </div>
            <div class = "button-serch">
                Сбросить
            </div>
        </div>
        <div id = "serchheader">

            <div class = "serchbox">
                <div class = 'serchcontent'>
                    <input  row = 'name' id = 'serch' placeholder = 'Название'>
                    <div row = 'name' class = 'remark'></div>
                </div>
            </div>

            <div class = "serchbox">
                <div class = 'serchcontent'>
                    <input  row = 'mark' placeholder = 'Марка'>
                    <div row = 'mark' class = 'remark'></div>
                </div>
            </div>

            <div class = "serchbox">
                <div class = 'serchcontent'>
                    <input row = 'model' placeholder = 'Модель'>
                    <div  row = 'model' class = 'remark'></div>
                </div>
            </div>

            <div class = "serchbox">
                <div class = 'serchcontent'>
                    <input row = 'kuzov'  placeholder = 'Кузов'>
                    <div row = 'kuzov' class = 'remark'></div>
                </div>
            </div>

            <div class = "serchbox">
                <div class = 'serchcontent'>
                    <input row = 'FR' placeholder = 'F/R'>
                    <div row = 'FR' class = 'remark'></div>
                </div>
            </div>

            <div class = "serchbox">
                <div class = 'serchcontent'>
                    <input row = 'LR' placeholder = 'L/R'>
                    <div row = 'LR' class = 'remark'></div>
                </div>
            </div>

            <div class = "serchbox">
                <div class = 'serchcontent' >
                    <input row = 'UD' id = 'serch' placeholder = 'U/D'>
                    <div row = 'UD' class = 'remark'></div>
                </div>
            </div>

            <div class = "serchbox">
                <div class = 'serchcontent'>
                    <input row = 'number'  placeholder = 'номер'>
                    <div row = 'number'  class = 'remark'></div>
                </div>
            </div>

            <div class = "serchbox">
                <div class = 'serchcontent'>
                    <input row = 'engine'  placeholder = 'Двигатель'>
                    <div row = 'engine' class = 'remark'></div>
                </div>
            </div>
        </div>
        <div id = "resultserch"></div>
    </body
</html>