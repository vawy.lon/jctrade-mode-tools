<?php

    function settings_get($line = NULL)
    {
        if(!file_exists("settings.json"))
        {
            $f = fopen("settings.json", 'w');
            fclose($f);
        }
        $text = file_get_contents("settings.json");
        $arr = json_decode($text, true);
        if($line == NULL) return $arr;
        return isset($arr[$line])?$arr[$line]:NULL;
    }
    function settings_set($line, $data)
    {
        $arr = settings_get();
        $arr[$line] = $data;
        $text = json_encode($arr, JSON_NUMERIC_CHECK);
        $f = fopen("settings.json", 'w');
        $f = fwrite($f, $text);
    }

?>