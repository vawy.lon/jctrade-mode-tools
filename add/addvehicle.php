<?php
session_start();
if(isset($_SESSION['accid']) && isset($_SESSION['adminlevel']) && $_SESSION['adminlevel'] > 0)
{
	include ($_SERVER['DOCUMENT_ROOT']."/data/phpcode/db.php");
	mysqli_query($mysql_line,"SET NAMES 'utf8'");
	$code = 0;
	if(!isset($_POST['v_vehicle']))
	{   
		$code = 0;
	}
	else if(!isset($_POST["v_boxid"]) || !strlen($_POST["v_boxid"]))
	{
		$code = 1;
	}
	else if(!isset($_POST["v_mark"]) || !strlen($_POST["v_mark"]))
	{
		$error = "Марка автомобиля не введена!";
		$code = 2;
	}
	else if(!isset($_POST["v_model"])|| !strlen($_POST["v_model"]))
	{
		$error = "Модель автомобиля не введён!";
		$code = 3;
	}
	else if(!isset($_POST["v_kuz"]) || !strlen($_POST["v_kuz"]))
	{
		$error = "Кузов автомобиля не введён!";
		$code = 4;
	}
	else if(!isset($_POST["v_eng"])|| !strlen($_POST["v_eng"]))
	{
		$error = "Двигатель автомобиля не введён!";
		$code = 5;
	}
	else if(!isset($_POST["v_god"])|| !strlen($_POST["v_god"]))
	{
		$error = "Год автомобиля не введён!";
		$code = 6;
	}

	else if(!isset($_POST["v_gaz"])|| !strlen($_POST["v_gaz"]))
	{
		$error = "Вид топлева не введён!";
		$code = 7;
	}
	else if(!isset($_POST["v_dsg"])|| !strlen($_POST["v_dsg"]))
	{
		$error = "Вид АКПП не введён!";
		$code = 8;
	}
	else if(!isset($_POST["v_vd"])|| !strlen($_POST["v_vd"]))
	{
		$error = "Вид АКПП не введён!";
		$code = 9;
	}

	else if(!isset($_POST["v_km"])|| !strlen($_POST["v_km"]))
	{
		$error = "Пробег не введён!";
		$code = 10;
	}

	else
	{
		// если событие (event) ровно set-img, то сохранить фото, если удалить, delete-img. Пусто - без изменений.
		$file_path_1 = '';
		$file_path_2 = '';
		if(isset($_POST['v_eventimg_1']) && $_POST['v_eventimg_1'] == 'set-img' && isset($_FILES['img_1']))
		{
			$file_path_1 = saveImage($_FILES['img_1']);
		}
		if(isset($_POST['v_eventimg_2']) && $_POST['v_eventimg_2'] == 'set-img' && isset($_FILES['img_2']))
		{
			$file_path_2 = saveImage($_FILES['img_2']);
		}
		
		$query = "INSERT INTO `adden` 
		(
			`box`, 
			`mark`, 
			`model`, 
			`kuz`, 
			`eng`, 
			`dsg`, 
			`god`, 
			`gaz`, 
			`vd`, 
			`km`, 
			`img_path_1`, 
			`img_path_2`
		) 
			VALUES 
		(
			'".$_POST["v_boxid"]."', 
			'".$_POST["v_mark"]."', 
			'".$_POST["v_model"]."', 
			'".$_POST["v_kuz"]."', 
			'".$_POST["v_eng"]."', 
			'".$_POST["v_dsg"]."', 
			'".$_POST["v_god"]."', 
			'".$_POST["v_gaz"]."', 
			'".$_POST["v_vd"]."', 
			'".$_POST["v_km"]."', 
			'".$file_path_1."', 
			'".$file_path_2."'
		);";

	$result = mysqli_query($mysql_line, $query);
	$code = 11;
	}
	echo $code;
}

function saveImage($file)
{
	if(isset($file) && $file['error'] == 0)
	{ 

		$path = $_SERVER['DOCUMENT_ROOT'].'/data/images/boxes';
		// Получаем расширение загруженного файла
		$extension = strtolower(substr(strrchr($file['name'], '.'), 1));
		 
		// Генерируем уникальное имя файла с этим расширением
		$filename = DFileHelper::getRandomFileName($path, $extension);
		 
		// Собираем адрес файла назначения
		$target = $path . '/' . $filename . '.' . $extension;
		// Загружаем 
		move_uploaded_file($file['tmp_name'], $target);
		return $filename . '.' . $extension;
		/*
		// Проверяем, загрузил ли пользователь файл
		$ext = substr($file['tmp_name'], strpos($file['tmp_name'],'.'), strlen($filename)-1); // В переменную $ext заносим 
		$filename = DFileHelper::getRandomFileName($path, $extension);


		$destiation_dir = 'vehicles/'.dirname('uploads') .'/'.$file['name']; // Директория для размещения файла
		move_uploaded_file($file['tmp_name'], $destiation_dir ); // Перемещаем файл в желаемую директорию
		//echo 'File Uploaded'; // Оповещаем пользователя об успешной загрузке файла
		*/
	}
	else
	{
		//echo 'No File Uploaded'; // Оповещаем пользователя о том, что файл не был загружен
		return 0;
	}
	return 0;
}

class DFileHelper
{
    public static function getRandomFileName($path, $extension='')
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';
 
        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($file));
 
        return $name;
    }
}

?>