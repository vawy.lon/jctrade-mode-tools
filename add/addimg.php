﻿<?php
    $data_inst = [];
    if(isset($_FILES['file_1'])) saveImage($_POST['name'], $_POST['fr'], $_POST['lr'], $_POST['ud'], $_POST['model'], 1, $_FILES['file_1']);
    if(isset($_FILES['file_2'])) saveImage($_POST['name'], $_POST['fr'], $_POST['lr'], $_POST['ud'], $_POST['model'], 2, $_FILES['file_2']);
    if(isset($_FILES['file_3'])) saveImage($_POST['name'], $_POST['fr'], $_POST['lr'], $_POST['ud'], $_POST['model'],  3, $_FILES['file_3']);
    if(isset($_FILES['file_4'])) saveImage($_POST['name'], $_POST['fr'], $_POST['lr'], $_POST['ud'], $_POST['model'],  4, $_FILES['file_4']);
    echo json_encode($data_inst);
    function saveImage($name, $fr, $lr, $ud, $model, $ind, $file)
    {
        global $data_inst;
        if(isset($file) && $file['error'] == 0)
        { 
            if($fr == "F") $fr = 'передний';
            if($fr == "R") $fr = 'задний';
            if($ud == "U") $fr = 'верхний';
            if($ud == "D") $fr = 'нижний';
            if($lr == "L") $fr = 'левый';
            if($lr == "R") $fr = 'правый';
            $time = date('Y-m-d H-i-s');
            $path = $_SERVER['DOCUMENT_ROOT'].'/images/';
            $extension = strtolower(substr(strrchr($file['name'], '.'), 1));
            $target = "{$name} {$model} {$fr} {$lr} {$ud}_{$ind} ({$time}.jpg";
            resize_photo($path,$target,$file['size'],$file['type'], $file['tmp_name']); 
            $data_inst['item'][$ind]['name'] = $name;
            $data_inst['item'][$ind]['model'] = $model;
            $data_inst['item'][$ind]['ud'] = $_POST['ud'];
            $data_inst['item'][$ind]['lr'] = $_POST['lr'];
            $data_inst['item'][$ind]['fr'] = $_POST['fr'];
            $data_inst['item'][$ind]['img'] = $target;

            return $target;
        }
        else
        {
            //echo 'No File Uploaded'; // Оповещаем пользователя о том, что файл не был загружен
            return 0;
        }
        return 0;
    }
    function resize_photo($path,$filename,$filesize,$type,$tmp_name){
        $quality = 60; //Качество в процентах. В данном случае будет сохранено 60% от начального качества.
        $size = 19485760; //Максимальный размер файла в байтах. В данном случае приблизительно 10 МБ.
        if($filesize<$size){
            switch($type){
                case 'image/jpeg': $source = imagecreatefromjpeg($tmp_name); break; //Создаём изображения по
                case 'image/png': $source = imagecreatefrompng($tmp_name); break;  //образцу загруженного  
                case 'image/gif': $source = imagecreatefromgif($tmp_name); break; //исходя из его формата
                default: {
                    return false;
                }
            }
            imagejpeg($source, $path.$filename, $quality); //Сохраняем созданное изображение по указанному пути в формате jpg
            include('img.php');
            $image = new SimpleImage();
            $image->load($path.$filename);
            $image->resize(900, 600);
            $image->save($path.$filename);
            imagedestroy($source);//Чистим память
            return true;
        }
        else {
            echo "fas";
            return false; 
        }    
    }
?>