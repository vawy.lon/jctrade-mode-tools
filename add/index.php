﻿<html prefix="og: http://ogp.me/ns#">
	<head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Автодиас - Фото</title>
        <script type="text/javascript" src="jquery.min.js"></script>
		<script type="text/javascript" src='jquery.cookie.js'></script>
    </head>
    <body>
<div class="windows8">
	<div class="wBall" id="wBall_1">
		<div class="wInnerBall"></div>
	</div>
	<div class="wBall" id="wBall_2">
		<div class="wInnerBall"></div>
	</div>
	<div class="wBall" id="wBall_3">
		<div class="wInnerBall"></div>
	</div>
	<div class="wBall" id="wBall_4">
		<div class="wInnerBall"></div>
	</div>
	<div class="wBall" id="wBall_5">
		<div class="wInnerBall"></div>
	</div>
</div>
        <div class = 'content-c'>
            <div class="block">
                <div class = 'addform'>
                    <div class = inputpanel>
                       <input type = text class = 'input-text' it = 'name'>
                       <div class = "podsc name">
                           <div class = 'content'>
                           </div>
                       </div>
                       <div class = 'remove'  it = 'name'>X</div>
                   </div>
                    <div class = frontpanel>
                        <div class = content-fr>
                            <div class = 'button' front = 'fr' id = front>Перед</div>
                            <div class = 'button' front = 'fr' id = back>Зад</div>
                        </div>
                        <div class = content-lr>
                            <div class = 'button ' front = 'lr' id = left>Лево</div>
                            <div class = 'button ' front = 'lr' id = right>Право</div>
                        </div>
                        <div class = content-ud>
                            <div class = 'button ' front = 'ud' id = up>Верх</div>
                            <div class = 'button ' front = 'ud' id = down>Низ</div>
                        </div>
                   </div>
                   <div class = inputpanel>
                        <input type = text class = 'input-text' it = 'model'>
                        <div class = "podsc model">
                           <div class = 'content'>
                           </div>
                       </div>
                        <div class = 'remove' it = 'model'>X</div>
                   </div>
                   <div class = imagepanel>
                        <div class = img-button>
                            <div class = 'buttondimg' handle = 1>X</div>
                            <div class = benterimg>
                                <input value = '' class = selectimg handle = 1 type = file>
                                <img class = 'previmg' handle = 1 src="addimg.png">
                            </div>
                        </div>
                        <div class = img-button>
                            <div class = 'buttondimg' handle = 2>X</div>
                            <div class = benterimg>
                                <input value = '' class = selectimg handle = 2 type = file>
                                <img class = 'previmg' handle = 2 src="addimg.png">
                            </div>
                       </div>
                       <div class = img-button>
                            <div class = 'buttondimg' handle = 3>X</div>
                            <div class = benterimg>
                                <input value = '' class = selectimg handle = 3 type = file>
                                <img class = 'previmg' handle = 3 src="addimg.png">
                            </div>
                       </div>
                       <div class = img-button>
                            <div class = 'buttondimg' handle = 4>X</div>
                            <div class = benterimg>
                                <input value = '' class = selectimg handle = 4 type = file>
                                <img class = 'previmg' handle = 4 src="addimg.png">
                            </div>
                       </div>
                   </div>
                   <div class = buttonpanel>
                       <div class = 'additem'>
                           Добавить
                       </div>
                   </div>
                   <div class = myitems>
                   </div>
                </div>
            </div>
        </div>
    </body>
</html>

<style>
    body .content-c{
        max-width: 370px;
        overflow: hidden;
        height: 900px;
    }
    .frontpanel{
        display: grid;
        grid-template-rows: 1fr;
        grid-template-columns: 1fr 1fr 1fr;
    }
    .content-fr{
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
    }
    .content-lr{
        display: flex;
        align-items: center;
    }
    .content-ud{
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
    }
    .benterimg{
        position: relative;
    }
    .buttondimg{
        background-color: #e0e0e0;
        border: 2px solid #929191;
        border-radius: 4px;
        width: 74px;
        text-align: center;
        text-transform: uppercase;
        margin: 5px;
        height: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 30px;
        height: 30px;
    }
    .button{
        background-color: #e0e0e0;
        border: 2px solid #929191;
        border-radius: 4px;
        width: 74px;
        text-align: center;
        text-transform: uppercase;
        margin: 5px;
        height: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .button.active{
        background-color: #abc1f1;
        border: 2px solid #65789b;
    }
    .input-text{
        margin: 5px;
        width: 75%;;
        font-size: 32px;
        background-color: #f7f7f7;
        border: navajowhite;
        text-align: center;
        box-shadow: 0px 0px 5px;
        border-radius: 5px;
    }
    .imagepanel{
        display: flex;
        justify-content: space-between;
        margin-bottom: 30px;
    }
    .img-button{
        width: 80px;
        height: 80px;
        display: flex;
        align-items: center;
        justify-content: center;
        position: relative;
        flex-direction: column;
    }
    .img-button input{
        position: absolute;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
    .img-button img{
        width: 30px;
    }
    .buttonpanel{
        display: flex;
        justify-content: center;
    }
    .additem{
        background-color: lightgreen;
        border: 3px solid green;
        width: 70%;
        font-size: 24px;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 10px;
        color: green;
    }
    .inputpanel{
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-bottom: 15px;
        margin-top: 15px;
    }
    .remove{
        width: 60px;
        background-color: #de5757;
        display: flex;
        align-items: center;
        height: 40px;
        justify-content: center;
        color: white;
        box-shadow: 0px 0px 6px black;
    }
    .inputpanel .podsc{
        overflow: auto;
        display: none;
        position: absolute;
        width: 95%;
        background-color: #f3f3f3;
        left: 13px;
        top: 56px;
        height: 200px;
        z-index: 1;
        border-radius: 10px;
        box-shadow: 0px 0px 10px black;
    }
    .inputpanel .podsc.active{
        display: block;
    }
    .inputpanel .podsc .content{
        padding: 10px
    }
    .inputpanel .podsc .content .items
    {
        text-align: center;
        font-size: 15px;
        background-color: white;
        border-radius: 5px;
        box-shadow: 0px 0px 5px;
        margin-bottom: 5px;
        padding: 5px;
    }
    .myitems .item{
        margin: 5px;
        background-color: #ececec;
        padding: 10px;
    }
.windows8 {
	position: relative;
	width: 40px;
	height:40px;
	margin:auto;
        display:none;
        position: fixed;
        top: 50vw;
        left: 50vw;
}
}

.windows8.active{
display:block;
}

.windows8 .wBall {
	position: absolute;
	width: 38px;
	height: 38px;
	opacity: 0;
	transform: rotate(225deg);
		-o-transform: rotate(225deg);
		-ms-transform: rotate(225deg);
		-webkit-transform: rotate(225deg);
		-moz-transform: rotate(225deg);
	animation: orbit 3.3225s infinite;
		-o-animation: orbit 3.3225s infinite;
		-ms-animation: orbit 3.3225s infinite;
		-webkit-animation: orbit 3.3225s infinite;
		-moz-animation: orbit 3.3225s infinite;
}

.windows8 .wBall .wInnerBall{
	position: absolute;
	width: 5px;
	height: 5px;
	background: rgb(16,23,22);
	left:0px;
	top:0px;
	border-radius: 5px;
}

.windows8 #wBall_1 {
	animation-delay: 0.726s;
		-o-animation-delay: 0.726s;
		-ms-animation-delay: 0.726s;
		-webkit-animation-delay: 0.726s;
		-moz-animation-delay: 0.726s;
}

.windows8 #wBall_2 {
	animation-delay: 0.143s;
		-o-animation-delay: 0.143s;
		-ms-animation-delay: 0.143s;
		-webkit-animation-delay: 0.143s;
		-moz-animation-delay: 0.143s;
}

.windows8 #wBall_3 {
	animation-delay: 0.2865s;
		-o-animation-delay: 0.2865s;
		-ms-animation-delay: 0.2865s;
		-webkit-animation-delay: 0.2865s;
		-moz-animation-delay: 0.2865s;
}

.windows8 #wBall_4 {
	animation-delay: 0.4295s;
		-o-animation-delay: 0.4295s;
		-ms-animation-delay: 0.4295s;
		-webkit-animation-delay: 0.4295s;
		-moz-animation-delay: 0.4295s;
}

.windows8 #wBall_5 {
	animation-delay: 0.583s;
		-o-animation-delay: 0.583s;
		-ms-animation-delay: 0.583s;
		-webkit-animation-delay: 0.583s;
		-moz-animation-delay: 0.583s;
}



@keyframes orbit {
	0% {
		opacity: 1;
		z-index:99;
		transform: rotate(180deg);
		animation-timing-function: ease-out;
	}

	7% {
		opacity: 1;
		transform: rotate(300deg);
		animation-timing-function: linear;
		origin:0%;
	}

	30% {
		opacity: 1;
		transform:rotate(410deg);
		animation-timing-function: ease-in-out;
		origin:7%;
	}

	39% {
		opacity: 1;
		transform: rotate(645deg);
		animation-timing-function: linear;
		origin:30%;
	}

	70% {
		opacity: 1;
		transform: rotate(770deg);
		animation-timing-function: ease-out;
		origin:39%;
	}

	75% {
		opacity: 1;
		transform: rotate(900deg);
		animation-timing-function: ease-out;
		origin:70%;
	}

	76% {
	opacity: 0;
		transform:rotate(900deg);
	}

	100% {
	opacity: 0;
		transform: rotate(900deg);
	}
}

@-o-keyframes orbit {
	0% {
		opacity: 1;
		z-index:99;
		-o-transform: rotate(180deg);
		-o-animation-timing-function: ease-out;
	}

	7% {
		opacity: 1;
		-o-transform: rotate(300deg);
		-o-animation-timing-function: linear;
		-o-origin:0%;
	}

	30% {
		opacity: 1;
		-o-transform:rotate(410deg);
		-o-animation-timing-function: ease-in-out;
		-o-origin:7%;
	}

	39% {
		opacity: 1;
		-o-transform: rotate(645deg);
		-o-animation-timing-function: linear;
		-o-origin:30%;
	}

	70% {
		opacity: 1;
		-o-transform: rotate(770deg);
		-o-animation-timing-function: ease-out;
		-o-origin:39%;
	}

	75% {
		opacity: 1;
		-o-transform: rotate(900deg);
		-o-animation-timing-function: ease-out;
		-o-origin:70%;
	}

	76% {
	opacity: 0;
		-o-transform:rotate(900deg);
	}

	100% {
	opacity: 0;
		-o-transform: rotate(900deg);
	}
}

@-ms-keyframes orbit {
	0% {
		opacity: 1;
		z-index:99;
		-ms-transform: rotate(180deg);
		-ms-animation-timing-function: ease-out;
	}

	7% {
		opacity: 1;
		-ms-transform: rotate(300deg);
		-ms-animation-timing-function: linear;
		-ms-origin:0%;
	}

	30% {
		opacity: 1;
		-ms-transform:rotate(410deg);
		-ms-animation-timing-function: ease-in-out;
		-ms-origin:7%;
	}

	39% {
		opacity: 1;
		-ms-transform: rotate(645deg);
		-ms-animation-timing-function: linear;
		-ms-origin:30%;
	}

	70% {
		opacity: 1;
		-ms-transform: rotate(770deg);
		-ms-animation-timing-function: ease-out;
		-ms-origin:39%;
	}

	75% {
		opacity: 1;
		-ms-transform: rotate(900deg);
		-ms-animation-timing-function: ease-out;
		-ms-origin:70%;
	}

	76% {
	opacity: 0;
		-ms-transform:rotate(900deg);
	}

	100% {
	opacity: 0;
		-ms-transform: rotate(900deg);
	}
}

@-webkit-keyframes orbit {
	0% {
		opacity: 1;
		z-index:99;
		-webkit-transform: rotate(180deg);
		-webkit-animation-timing-function: ease-out;
	}

	7% {
		opacity: 1;
		-webkit-transform: rotate(300deg);
		-webkit-animation-timing-function: linear;
		-webkit-origin:0%;
	}

	30% {
		opacity: 1;
		-webkit-transform:rotate(410deg);
		-webkit-animation-timing-function: ease-in-out;
		-webkit-origin:7%;
	}

	39% {
		opacity: 1;
		-webkit-transform: rotate(645deg);
		-webkit-animation-timing-function: linear;
		-webkit-origin:30%;
	}

	70% {
		opacity: 1;
		-webkit-transform: rotate(770deg);
		-webkit-animation-timing-function: ease-out;
		-webkit-origin:39%;
	}

	75% {
		opacity: 1;
		-webkit-transform: rotate(900deg);
		-webkit-animation-timing-function: ease-out;
		-webkit-origin:70%;
	}

	76% {
	opacity: 0;
		-webkit-transform:rotate(900deg);
	}

	100% {
	opacity: 0;
		-webkit-transform: rotate(900deg);
	}
}

@-moz-keyframes orbit {
	0% {
		opacity: 1;
		z-index:99;
		-moz-transform: rotate(180deg);
		-moz-animation-timing-function: ease-out;
	}

	7% {
		opacity: 1;
		-moz-transform: rotate(300deg);
		-moz-animation-timing-function: linear;
		-moz-origin:0%;
	}

	30% {
		opacity: 1;
		-moz-transform:rotate(410deg);
		-moz-animation-timing-function: ease-in-out;
		-moz-origin:7%;
	}

	39% {
		opacity: 1;
		-moz-transform: rotate(645deg);
		-moz-animation-timing-function: linear;
		-moz-origin:30%;
	}

	70% {
		opacity: 1;
		-moz-transform: rotate(770deg);
		-moz-animation-timing-function: ease-out;
		-moz-origin:39%;
	}

	75% {
		opacity: 1;
		-moz-transform: rotate(900deg);
		-moz-animation-timing-function: ease-out;
		-moz-origin:70%;
	}

	76% {
	opacity: 0;
		-moz-transform:rotate(900deg);
	}

	100% {
	opacity: 0;
		-moz-transform: rotate(900deg);
	}
}
</style>

<script>
    $(document).on('click', '.button', function(){
        var front = $(this).attr('front')
        if($(this).hasClass('active'))
        {
            $(this).removeClass('active');
        }
        else
        {
            $(".button[front='"+front+"'").removeClass('active');
            $(this).addClass('active');
        }
    })
    $(document).on('click', '.remove', function(){
        var it = $(this).attr('it')
        $('.input-text[it="'+it+'"]').val('');
    })

    $(document).on('change', ".selectimg", function()
    {
        var name = $(".input-text[it='name']").val()
        var handle = $(this).attr('handle');
        var handle = $(this).attr('handle')
        var reader = new FileReader();
        reader.onload = function(e) 
        {
            $('.previmg[handle="'+handle+'"]').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
        var reader = new FileReader();
    });
    $(document).on('click', '.buttondimg', function(){
        var handle = $(this).attr('handle');
        removeimg(handle)
    })
    function removeimg(ind)
    {
        $(".selectimg[handle='"+ind+"']").val('')
        $(".previmg[handle='"+ind+"']").attr('src', 'addimg.png')
    }
    $(document).on('click', '.additem', function()
    {
        $('.windows8').addClass('active')

        var rtl = $(this);

        rtl.html('Загрузка...')

        $.ajax(
        {
            type: 'POST',
            url: 'addimg.php',
            data:  getData(),
            processData: false,
            contentType: false,
            dataType: "text",
            success: function(data) 
            {
                removeimg(1)
                removeimg(2)
                removeimg(3)
                removeimg(4)
                additem(JSON.parse(data)['item'])
                $('.windows8').removeClass('active')
                rtl.html('Добавить')
            },
            error: function(data) 
            {
                $('.windows8').removeClass('active')
                rtl.html('Добавить')
            }
        });
    })
    function additem(items)
    {
        $.each(items, function(i, item)
        {
            console.log(item)
            var fr = '';
            if(item['fr'] == 'F') fr = 'передняя';
            else if(item['fr'] == 'R') fr = 'задняя';
            else fr = '';


            var lr = '';
            if(item['lr'] == 'L') lr = 'левая';
            else if(item['lr'] == 'R') lr = 'правая';
            else lr = '';

            var ud = '';
            if(item['ud'] == 'U') ud = 'верхняя';
            else if(item['ud'] == 'D') ud = 'нижняя';
            else ud = '';
            
            $('.myitems').prepend
            ("\
                    <div class = item>\
                        <div class = name>\
                        Название: "+item['name']+"<br>\
                        Марка: "+item['model']+"<br>\
                        Сторона: "+fr+" "+lr+" "+ ud +"\
                        </div>\
                </div>\
            ")
        })
    }
    function getData()
    {
        var data = new FormData();
        data.append('name', $('.input-text[it="name"]').val());
        data.append('model', $('.input-text[it="model"]').val());

        if($('.button#front').hasClass('active')) data.append('fr', 'F');
        else if($('.button#back').hasClass('active')) data.append('fr', 'R');
        else data.append('fr', '');

        if($('.button#left').hasClass('active'))data.append('lr', 'L');
        else if($('.button#right').hasClass('active')) data.append('lr', 'R');
        else data.append('lr', '');

        if($('.button#up').hasClass('active')) data.append('ud', 'U');
        else if($('.button#down').hasClass('active')) data.append('ud', 'D');
        else data.append('ud', '');

        data.append('file_1', $('.selectimg[handle="1"]').prop('files')[0])
        data.append('file_2', $('.selectimg[handle="2"]').prop('files')[0])
        data.append('file_3', $('.selectimg[handle="3"]').prop('files')[0])
        data.append('file_4', $('.selectimg[handle="4"]').prop('files')[0])
        return data;
    }
    $(document).on('focusout', '.input-text', function(){

        var int = $(this).siblings(".podsc");
        setTimeout(function(){
            int.removeClass('active');
        }, 300);
    })
    $(document).on('focusin', '.input-text', function(){
        $(this).siblings(".podsc").addClass('active');
    })
    $(document).on('click', '.items', function(){
        var it = $(this).attr('it')
        $(".input-text[it='"+it+"']").val($(this).html());
    })
    $(document).on('input', '.input-text[it=name]', function(){
        
        $.get('getnames.php', {'name': $(this).val()}, function(data){
            var content = $(".podsc.name .content");
            content.html("");
            var arr = JSON.parse(data);
            $.each(arr, function(i, v){
                content.append("<div class = 'items name' it = 'name'>"+v['n']+"</div>")
            })
        })
    })

    $(document).on('input', '.input-text[it=model]', function(){
        
        var line = $(this).val()
        var arg = line.split(' ')
        var args = arg.length;
        var mark = '';
        var model = '';
        var kuz = '';

        if(args == 1) kuz = arg[0];
        if(args == 2) 
        {
            mark = arg[0];
            model = arg[1];
        }
        if(args == 3) 
        {
            mark = arg[0];
            model = arg[1];
            kuz = arg[2];
        }
        $.post('serchmodel.php', {mark: mark, model: model, kuzov: kuz, args: args}, function(data){
            var arr = JSON.parse(data);
            var content = $(".podsc.model .content");
            content.html("");
            var arr = JSON.parse(data);
            $.each(arr, function(i, v){
                content.append("<div class = 'items' it = 'model'>"+v['mark']+" "+v['model']+" "+v['kuzov']+"</div>")
            })
        })
    })

</script>